#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:2a5b2c48f514736548978e30b1a033b37836d15c; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:53f6c5d9e5c9d9d101ec3ddc5f7b68953ec44813 \
          --target EMMC:/dev/block/by-name/recovery:134217728:2a5b2c48f514736548978e30b1a033b37836d15c && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
